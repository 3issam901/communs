package tn.smartdev.commons.exception;

import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

/**
 * 
 * @author arbi
 *
 */
public class EmailAsyncUncaughtExceptionHandler implements AsyncUncaughtExceptionHandler {

	private static final Logger LOGGER = LogManager.getLogger(EmailAsyncUncaughtExceptionHandler.class);

	@Override
	public void handleUncaughtException(Throwable throwable, Method method, Object... objects) {
		if ("sendHtmlEmail".equals(method.getName())) {
			if (LOGGER.isErrorEnabled()) {
				LOGGER.error("Un probleme est survenu dans la methode {}, lors de l'envoi asynchrone de l'email : {}",
						method, objects);
			}
		}

	}

}
