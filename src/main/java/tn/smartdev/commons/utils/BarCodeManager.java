package tn.smartdev.commons.utils;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Reader;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.QRCodeReader;

public class BarCodeManager {

	public static void marshallBarCodeImage(File file, String qrCodeText, int size, String fileType)
			throws WriterException, IOException {
		BitMatrix bitMatrix;
		bitMatrix = new Code128Writer().encode(qrCodeText, BarcodeFormat.CODE_128, size * 2, size, null);
		MatrixToImageWriter.writeToStream(bitMatrix, fileType, new FileOutputStream(file));
		System.out.println("Code128 Barcode Generated.");
	}

	public static String unmarshallBarCodeImage(File file,String fileType)
			throws NotFoundException, ChecksumException, FormatException, FileNotFoundException, IOException {
		Image imageAwt = ImageIO.read(file);
		File imgFile = File.createTempFile(Calendar.getInstance().getTime().getTime() + "", null);
		ImageIO.write((RenderedImage) imageAwt, fileType, imgFile);
		BufferedImage image = ImageIO.read(imgFile);
		BinaryBitmap bitmap = null;
		Result result = null;

		int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
		RGBLuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), pixels);
		bitmap = new BinaryBitmap(new HybridBinarizer(source));

		String resultText = null;
		if (bitmap != null) {
			Code128Reader reader = new Code128Reader();
			result = reader.decode(bitmap);
			resultText = result.getText();
		}
		return resultText;

	}

}
