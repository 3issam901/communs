package tn.smartdev.commons.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id$
 * @since 1.0 - 18 dec. 2013 - 11:42:55
 */
public final class FileManager {
	private static final char EXTENSION_SEPARATOR = '.';
	private static final char UNIX_SEPARATOR = '/';
	private static final char WINDOWS_SEPARATOR = '\\';

	private static int indexOfLastSeparator(String filename) {
		if (filename == null) {
			return -1;
		}
		int lastUnixPos = filename.lastIndexOf(UNIX_SEPARATOR);
		int lastWindowsPos = filename.lastIndexOf(WINDOWS_SEPARATOR);
		return Math.max(lastUnixPos, lastWindowsPos);
	}

	public static int indexOfExtension(String filename) {
		if (filename == null) {
			return -1;
		}
		int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
		int lastSeparator = indexOfLastSeparator(filename);
		return (lastSeparator > extensionPos ? -1 : extensionPos);
	}

	public static String getExtension(String filename) {
		if (filename == null) {
			return null;
		}
		int index = indexOfExtension(filename);
		if (index == -1) {
			return "";
		} else {
			return filename.substring(index + 1);
		}
	}

	public static void moveFilesToFolder(String folderPath, File[] files) throws IOException {
		for (File file : files) {
			FileManager.moveFileToFolder(folderPath, file);
		}
	}

	public static void moveFileToFolder(String folderPath, File file) throws IOException {
		// Contrôle du chemin
		forceMkdir(folderPath);

		boolean success = file.renameTo(new File(folderPath, file.getName()));
		if (!success) {
			success = file.renameTo(new File(folderPath, file.getName() + ".double"));

			if (!success) {
				throw new IOException("Le deplacement de fichier " + file.getName() + " a echoue.");
			}
		}
	}

	/**
	 * Deplace des fichiers dans un repertoire
	 *
	 * @param folderPath
	 *            Repertoire de destination
	 * @param files
	 *            tableau de fichiers à deplacer
	 *
	 * @throws Exception
	 *             Si le deplacement echoue
	 */
	public static void moveFilesToFolder(File folderPath, File[] files) throws Exception {
		// Pour chaque fichier, on les deplaces dans les repertoires
		for (File file : files) {
			FileManager.moveFileToFolder(folderPath, file);
		}
	}

	/**
	 * Deplace un fichier dans un repertoire
	 *
	 * @param folderPath
	 *            Chemin du repertoire de destination
	 * @param file
	 *            Fichier à deplacer
	 *
	 * @throws Exception
	 *             si le deplacement de fichier echoue ou si le fichier existe
	 *             dejà dans le repertoire de destination
	 */
	public static void moveFileToFolder(File folderPath, File file) throws Exception {

		// Deplace (renameTo) le fichier dans le nouveau repertoire
		boolean success = file.renameTo(new File(folderPath, file.getName()));
		// Si ça n'as pas marche, on le renomme avec une autre extension
		if (!success) {
			success = file.renameTo(new File(folderPath, file.getName() + ".double"));

			if (!success) {
				// Si malgre tout ça n'a pas marche, on lance une exception
				throw new IOException("Le deplacement de fichier " + file.getName() + " a echoue.");
			}
		}
	}

	/**
	 * Contrôle le chemin d'un repertoire et le cree s'il n'existe pas
	 *
	 * @param folderName
	 *            chemin du repertoire à controler
	 *
	 * @throws IOException
	 *             Si une erreur de lecture ou d'ecriture
	 * 
	 */
	public static void forceMkdir(String folderName) throws IOException {
		File directory = new File(folderName);
		if (directory.exists()) {
			if (!directory.isDirectory()) {
				String message = "File " + directory + " exists and is "
						+ "not a directory. Unable to create directory.";
				throw new IOException(message);
			}
		} else {
			if (!directory.mkdirs()) {
				// Double-check that some other thread or process hasn't made
				// the directory in the background
				if (!directory.isDirectory()) {
					String message = "Unable to create directory " + directory;
					throw new IOException(message);
				}
			}
		}
	}

	public static String getHostName() throws UnknownHostException {
		return InetAddress.getLocalHost().getHostAddress() + "(" + InetAddress.getLocalHost().getHostName() + ")";
	}

	public static String readFile(File file) throws IOException {
		String fileContent = "";
		FileReader fileReader = new FileReader(file.getAbsolutePath());
		// Always wrap FileReader in BufferedReader.
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		// get file details and get info you need.
		while ((line = bufferedReader.readLine()) != null) {
			fileContent += (line + "\n");
		}
		bufferedReader.close();
		return fileContent;
	}
	public static String readFile(String pathInClasspath) throws IOException, URISyntaxException{
		return Files.readAllLines(
			    Paths.get(FileManager.class.getClass().getResource(pathInClasspath).toURI()), Charset.defaultCharset()).toString();
	}

	public static void writeFile(String filePath, String fileContent) throws IOException {
		File file = new File(filePath);

		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(fileContent);
		bw.close();

	}

	public static boolean deleteFile(String filePath) {
		File file = new File(filePath);

		if (file.delete()) {
			return true;
		} else {
			return false;
		}
	}

	public static File getFileFromClassPath(String path) {
		try {
			ClassLoader classLoader = FileManager.class.getClassLoader();
			return new File(classLoader.getResource(path).toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}
}
