package tn.smartdev.commons.utils;

/**
 * @author issam benbelgacem
 * @version 1.0
 * 
 */

public class StringUtils {

	/**
	 * 
	 * @param stringBuilder
	 * @param oldString
	 * @param newString
	 * @return void
	 * 
	 */
	public static void ReplaceBuffer(StringBuilder stringBuilder, String oldString, String newString) {
		int start = stringBuilder.indexOf(oldString),end=0;
		while (start != -1) {
			end = start + oldString.length();
			stringBuilder.replace(start, end, newString);
			start = stringBuilder.indexOf(oldString);
		}
	}

	public static void main(String[] args) {
		StringBuilder str = new StringBuilder("Util Java Util Package Util Util Util");
		ReplaceBuffer(str, "Util", "Lang");
		System.out.println(str);
	}
}
