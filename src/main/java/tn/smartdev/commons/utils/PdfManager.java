package tn.smartdev.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.List;

import org.ghost4j.document.PDFDocument;
import org.ghost4j.renderer.RendererException;
import org.ghost4j.renderer.SimpleRenderer;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class PdfManager {
	public static void writeFromHtml(OutputStream out, String html) throws Exception {
		writeFromHtml(out, html, null);
	}

	public static void writeFromHtml(OutputStream out, String html, File imageFile) throws Exception {
		Document document = new Document();
		PdfWriter.getInstance(document, out);
		document.open();
		if (imageFile != null && imageFile.exists()) {
			Image image = Image.getInstance(imageFile.getAbsolutePath());
			image.setAbsolutePosition(document.getPageSize().getWidth() - image.getWidth(),
					document.getPageSize().getHeight() - image.getHeight());
			image.setAlignment(2);
			document.add(image);

		}
		HTMLWorker htmlWorker = new HTMLWorker(document);
		htmlWorker.parse(new StringReader(html));
		document.close();
	}

	public static void writeTXT(Document document, String... lines) throws DocumentException {
		Paragraph preface = new Paragraph();
		for (int i = 0; i < lines.length; i++) {
			preface.add(new Paragraph(lines[i]));
		}

		document.add(preface);
		// Start a new page
		document.newPage();
	}

	public static void addingCodeToEachPage(File in, File out, File imagefile)
			throws FileNotFoundException, IOException, DocumentException {
		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(out));
		document.open();

		PdfContentByte cb = writer.getDirectContent();

		// Load existing PDF
		PdfReader reader = new PdfReader(new FileInputStream(in));
		for (int i = 0; i < reader.getNumberOfPages(); i++) {
			PdfImportedPage page = writer.getImportedPage(reader, i + 1);
			if (i > 0)
				document.newPage();
			cb.addTemplate(page, 0, 0);
			Image jpg = Image.getInstance(imagefile.getAbsolutePath());
			jpg.setAlignment(2);
			document.add(jpg);
		}
		document.close();
	}
	/**
	 * To use this method you need to add to class path a dll for GS
	 * wind gsll.dll
	 * win64 gsdll64.dll
	 * 
	 * @param pdfFile
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws RendererException
	 * @throws org.ghost4j.document.DocumentException
	 */
	public static List<java.awt.Image> split(File pdfFile)
			throws FileNotFoundException, IOException, RendererException, org.ghost4j.document.DocumentException {
		PDFDocument document = new PDFDocument();
		document.load(pdfFile);
		SimpleRenderer renderer = new SimpleRenderer();
		renderer.setResolution(300);
		return renderer.render(document);

	}
}
