package tn.smartdev.commons.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: ModelObject.java 9165 2015-07-22 13:05:23Z arbi $
 * @since 1.0 - 17 janv. 2013 - 14:27:22
 */
@MappedSuperclass
public abstract class ModelObject<S extends Serializable> implements Model<S> {

	public abstract S getId();

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATING_DATE")
	private Date updatingDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DELETING_DATE")
	private Date deletingDate;

	public void setDeletingDate(Date deletingDate) {
		this.deletingDate = deletingDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getDeletingDate() {
		return deletingDate;
	}

	public Date getUpdatingDate() {
		return updatingDate;
	}

	public void setUpdatingDate(Date updatingDate) {
		this.updatingDate = updatingDate;
	}

}
