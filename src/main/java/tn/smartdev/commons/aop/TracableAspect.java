package tn.smartdev.commons.aop;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import tn.smartdev.commons.log.LoggerService;
import tn.smartdev.commons.log.LoggerService.ENVIRONEMENT;
import tn.smartdev.commons.log.LoggerService.LEVEL;
import tn.smartdev.commons.utils.FileManager;

/**
 * cet aspect monitor l'entree
 * 
 * @author walid Mansia
 *
 */
@Aspect
public class TracableAspect {
	@Autowired
	private LoggerService loggerService;
	@Value("${aop.monitoring.duration}")
	private int monitoringDuration;
	private Date startDate;
	private Object identityAop;

	public TracableAspect() {
		this.startDate = new Date();
	}

	public void setIdentityAop(Object identityAop) {
		this.identityAop = identityAop;
	}

	@Around("@annotation(tn.smartdev.commons.aop.Tracable)")
	// @Around("execution(* tn.*.service.*.*(..))")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long start = System.currentTimeMillis();
		Object result = point.proceed();
		if (isMonitoringActive()) {
			long time = System.currentTimeMillis() - start;
			String identifier = identityAop == null ? "NO IDENTIFIER" : identityAop.toString();
			// System.err.println(identifier+" //Signature : "
			// +point.getTarget().getClass().getCanonicalName()+"."
			// + MethodSignature.class.cast(point.getSignature()).getMethod()
			// .getName() + ", Args : " + point.getArgs()
			// + ", Result:" + result + ", executionTime:" + time);
			MonitorMessage message = new MonitorMessage();
			message.setApplicationName(loggerService.getApplicationName());
			message.setArgs(point.getArgs() + "");
			message.setEnvironement(loggerService.getENVIRONEMENT());
			message.setLevel(loggerService.getLEVEL());
			message.setMessage("");// TODO
			message.setHost(FileManager.getHostName());
			message.setMethode(point.getTarget().getClass().getCanonicalName() + "."
					+ MethodSignature.class.cast(point.getSignature()).getMethod().getName());
			message.setResult(result + "");
			message.setTime(time);
			message.setIdentifier(identifier);
			loggerService.monitor(message);
		}
		return result;
	}

	private boolean isMonitoringActive() {
		return startDate.getTime() + monitoringDuration * 60 * 1000 > new Date().getTime();
	}

	public void reset() {
		startDate = new Date();
	}

	final static class MonitorMessage {
		private String identifier;
		private String applicationName;
		private LEVEL level;
		private ENVIRONEMENT environement;
		private String host;
		private String methode;
		private String message;
		private String sourceURL;
		private long time;
		private String args;
		private String result;

		public String getIdentifier() {
			return identifier;
		}

		public void setIdentifier(String identifier) {
			this.identifier = identifier;
		}

		public String getArgs() {
			return args;
		}

		public String getResult() {
			return result;
		}

		public long getTime() {
			return time;
		}

		public void setArgs(String args) {
			this.args = args;
		}

		public void setResult(String result) {
			this.result = result;
		}

		public void setTime(long time) {
			this.time = time;
		}

		public String getApplicationName() {
			return applicationName;
		}

		public void setApplicationName(String applicationName) {
			this.applicationName = applicationName;
		}

		public LEVEL getLevel() {
			return level;
		}

		public void setLevel(LEVEL level) {
			this.level = level;
		}

		public ENVIRONEMENT getEnvironement() {
			return environement;
		}

		public void setEnvironement(ENVIRONEMENT environement) {
			this.environement = environement;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getMethode() {
			return methode;
		}

		public void setMethode(String methode) {
			this.methode = methode;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getSourceURL() {
			return sourceURL;
		}

		public void setSourceURL(String sourceURL) {
			this.sourceURL = sourceURL;
		}

		@Override
		public String toString() {

			return "super: " + super.toString() + ", getApplicationName:" + getApplicationName() + ", getLevel:"
					+ getLevel() + ", getEnvironement:" + getEnvironement() + ", getHost:" + getHost() + ", getMethode:"
					+ getMethode() + ", getMessage:" + getMessage() + ", getSourceURL:" + getSourceURL();
		}

	}
}
