package tn.smartdev.commons.security;

import java.util.Date;

import tn.smartdev.commons.security.blowfish.BlowfishImpl;
/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 21 aout 2013 - 13:56:22
 */
public class CryptUtils {

    public static String decrypt(Date key, String password) throws Exception {
        BlowfishImpl blowFish = new BlowfishImpl();
        return blowFish.decrypt(key, password);
    }

    public static String encrypt(Date key, String password) throws Exception {
        BlowfishImpl blowFish = new BlowfishImpl();
        return blowFish.encrypt(key, password);
    }
}
