package tn.smartdev.commons.email;

import java.io.File;
import java.util.List;

public final class MyEmailMessage {
	private final String to;
	private final List<String> ccs;
	private final List<String> ccis;
	private final String object;
	private final String htmlMessage;
	private final List<File> files;

	public MyEmailMessage(String to, List<String> ccs, List<String> ccis, String object, String htmlMessage,
			List<File> files) {
		super();
		this.to = to;
		this.ccs = ccs;
		this.ccis = ccis;
		this.object = object;
		this.htmlMessage = htmlMessage;
		this.files = files;
	}

	public String getTo() {
		return to;
	}

	public List<String> getCcs() {
		return ccs;
	}

	public List<String> getCcis() {
		return ccis;
	}

	public String getObject() {
		return object;
	}

	public String getHtmlMessage() {
		return htmlMessage;
	}

	public List<File> getFiles() {
		return files;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{ to : ");
		stringBuilder.append(getTo());
		stringBuilder.append(", ccs : ");
		stringBuilder.append(getCcs());
		stringBuilder.append(", cci : ");
		stringBuilder.append(getCcis());
		stringBuilder.append(", object : ");
		stringBuilder.append(getObject());
		stringBuilder.append(", htmlMessage : ");
		stringBuilder.append(getHtmlMessage());
		stringBuilder.append(", files : ");
		stringBuilder.append(getFiles());
		stringBuilder.append(" }");
		return stringBuilder.toString();
	}
}
