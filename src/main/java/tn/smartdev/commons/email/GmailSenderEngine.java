package tn.smartdev.commons.email;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import tn.smartdev.commons.exception.TechnicalException;

@Service
public class GmailSenderEngine implements EmailSenderFacade {

	private static final Logger LOGGER = LogManager.getLogger(GmailSenderEngine.class);
	/**
	 * attrs
	 */
	private final Properties properties;
	private boolean disabled=false;
	public GmailSenderEngine(@Autowired Environment environment) {
		this.properties = new Properties();
		properties.put("username", environment.getRequiredProperty("mail.user"));
		properties.put("password", environment.getRequiredProperty("mail.password"));
		properties.put("mail.smtp.auth", environment.getRequiredProperty("mail.smtp.auth"));
		properties.put("mail.smtp.starttls.enable", environment.getRequiredProperty("mail.smtp.starttls.enable"));
		properties.put("mail.smtp.host", environment.getRequiredProperty("mail.smtp.host"));
		properties.put("mail.smtp.port", environment.getRequiredProperty("mail.smtp.port"));
		if(environment.getProperty("mail.disabled") != null){
			disabled=Boolean.parseBoolean(environment.getProperty("mail.disabled"));
		}
	}

	@Async
	@Override
	public void sendHtmlEmail(String tos, List<String> ccs, List<String> ccis, String object, String htmlMessage,
			List<File> files) throws TechnicalException {
		LOGGER.info("Sending mail : [{}, {}, {}, {}, {}, {}]", tos, ccs, ccis, object, htmlMessage, files);
		final MyEmailMessage myEmailMessage = new MyEmailMessage(tos, ccs, ccis, object, htmlMessage, files);
		try {
			sendMessage(myEmailMessage);
		} catch (MessagingException messagingException) {
			LOGGER.error("Error sending mail ", messagingException);
			throw new TechnicalException("Un probleme est survenu lors de l'envoi de l'email",
					messagingException.fillInStackTrace());
		}
	}

	@Override
	public void sendHtmlEmail(String tos, String object, String htmlMessage) throws TechnicalException {
		sendHtmlEmail(tos, null, null, object, htmlMessage, null);
	}

	private void sendMessage(MyEmailMessage myEmailMessage) throws MessagingException {
		if(disabled){
			sendConsoleMessage(myEmailMessage);			
		}else{
			sendSmtpMessage(myEmailMessage);
		}
	}
	private void sendConsoleMessage(MyEmailMessage myEmailMessage){
		LOGGER.info("TO: "+myEmailMessage.getTo());
		LOGGER.info("CCI: "+myEmailMessage.getCcis());
		LOGGER.info("CC: "+myEmailMessage.getCcs());
		LOGGER.info("OBJECT: "+myEmailMessage.getObject());
		LOGGER.info("MESSAGE: "+myEmailMessage.getHtmlMessage());
		
	}
	
	private void sendSmtpMessage(MyEmailMessage myEmailMessage) throws MessagingException {
		final Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getProperty("username"),
						properties.getProperty("password"));
			}
		});
		final MimeMessage message = new MimeMessage(session);
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(myEmailMessage.getTo()));
		message.setSubject(myEmailMessage.getObject());
		message.setContent(myEmailMessage.getHtmlMessage(), " text/html");
		Transport.send(message);
	}

}
