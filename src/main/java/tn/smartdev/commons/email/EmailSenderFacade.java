package tn.smartdev.commons.email;

import java.io.File;
import java.util.List;

import tn.smartdev.commons.exception.TechnicalException;

public interface EmailSenderFacade {

	/**
	 * Send Html E-mail
	 * 
	 * @param tos
	 *            receivers
	 * @param ccs
	 * @param ccis
	 * @param object
	 * @param htmlMessage
	 * @param files
	 * @throws TechnicalException
	 */
	void sendHtmlEmail(String tos, List<String> ccs, List<String> ccis, String object, String htmlMessage,
			List<File> files) throws TechnicalException;

	void sendHtmlEmail(String tos, String object, String htmlMessage) throws TechnicalException;
}
