package tn.smartdev.commons.log;

public interface LoggerService {
	enum LEVEL {
		TRACE(1),DEBUG(2),INFO(3),WARN(4),ERROR(5);
		private int value;

		private LEVEL(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

	}
	enum ENVIRONEMENT{
		LOCAL,RECETTE,PROD
	}
	public ENVIRONEMENT getENVIRONEMENT();
	public String getApplicationName();
	public LEVEL getLEVEL();
	public void setLEVEL(LEVEL level);
	public void trace(String method,String message);
	public void debug(String method,String message);
	public void info(String method,String message);
	public void warn(String method,String message);
	public void error(String method,String message);
	public void monitor(Object object);
}
