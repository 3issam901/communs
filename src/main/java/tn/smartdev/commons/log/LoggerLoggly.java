package tn.smartdev.commons.log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tn.smartdev.commons.utils.FileManager;

@Service
public class LoggerLoggly implements LoggerService {

	private static final Logger LOGGER = LogManager.getLogger(LoggerLoggly.class);
	private static final String REST_URL = "http://logs-01.loggly.com/inputs/b46d812b-a494-42e5-ad9c-9b6feafa9f74/tag/http/";

	/**
	 * attrs
	 */
	private final String applicationName;
	private LEVEL level;
	private final ENVIRONEMENT environement;;

	public LoggerLoggly(@Autowired Environment env) {
		this.applicationName = env.getProperty("log.applicationName");
		this.level = env.getProperty("log.level", LEVEL.class);
		this.environement = env.getProperty("log.environement", ENVIRONEMENT.class);
	}

	private void log(String method, LEVEL level, String message) {
		if (getLEVEL().getValue() > level.getValue())
			return;// ne rien faire si le log a un niveau strictement inferieur
					// au niveau défini
		String jSonMessage;
		LogMessage logMessage = new LogMessage();
		logMessage.setApplicationName(applicationName);
		logMessage.setLevel(level);
		logMessage.setEnvironement(getENVIRONEMENT());
		try {
			logMessage.setHost(FileManager.getHostName());
		} catch (UnknownHostException e) {
			logMessage.setHost("inconnu");
			LOGGER.error(e);
		}
		logMessage.setMethode(method);
		logMessage.setMessage(message);
		logMessage.setSourceURL("");// a definir
		final ObjectMapper mapper = new ObjectMapper();
		try {
			jSonMessage = mapper.writeValueAsString(logMessage);
		} catch (JsonProcessingException e) {
			jSonMessage = logMessage.toString() + ", serializez exception :" + e.getMessage();
			LOGGER.error(e);
		}
		try {
			if (ENVIRONEMENT.PROD.compareTo(getENVIRONEMENT()) == 0) {
				sendMessage(jSonMessage);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	private void sendMessage(String payload) throws Exception {
		final StringBuffer jsonString = new StringBuffer();
		try {
			final URL url = new URL(getRestUrl());
			final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Headers", "content-type:application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
			writer.write(payload);
			writer.close();
			final BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				jsonString.append(line);
			}
			br.close();
			connection.disconnect();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	private final static class LogMessage {
		private String applicationName;
		private LEVEL level;
		private ENVIRONEMENT environement;
		private String host;
		private String methode;
		private String message;
		private String sourceURL;

		public String getApplicationName() {
			return applicationName;
		}

		public void setApplicationName(String applicationName) {
			this.applicationName = applicationName;
		}

		public LEVEL getLevel() {
			return level;
		}

		public void setLevel(LEVEL level) {
			this.level = level;
		}

		public ENVIRONEMENT getEnvironement() {
			return environement;
		}

		public void setEnvironement(ENVIRONEMENT environement) {
			this.environement = environement;
		}

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getMethode() {
			return methode;
		}

		public void setMethode(String methode) {
			this.methode = methode;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getSourceURL() {
			return sourceURL;
		}

		public void setSourceURL(String sourceURL) {
			this.sourceURL = sourceURL;
		}

		@Override
		public String toString() {

			return "super: " + super.toString() + ", getApplicationName:" + getApplicationName() + ", getLevel:"
					+ getLevel() + ", getEnvironement:" + getEnvironement() + ", getHost:" + getHost() + ", getMethode:"
					+ getMethode() + ", getMessage:" + getMessage() + ", getSourceURL:" + getSourceURL();
		}
	}

	public String getRestUrl() {
		return REST_URL;
	}

	@Override
	public ENVIRONEMENT getENVIRONEMENT() {
		return this.environement;
	}

	@Override
	public String getApplicationName() {
		return applicationName;
	}

	@Override
	public LEVEL getLEVEL() {
		return level;
	}

	@Override
	public void trace(String method, String message) {
		log(method, LEVEL.TRACE, message);
		LOGGER.trace(method + "/" + message);
	}

	@Override
	public void debug(String method, String message) {
		log(method, LEVEL.DEBUG, message);
		LOGGER.debug(method + "/" + message);
	}

	@Override
	public void info(String method, String message) {
		log(method, LEVEL.INFO, message);
		LOGGER.info(method + "/" + message);
	}

	@Override
	public void warn(String method, String message) {
		log(method, LEVEL.WARN, message);
		LOGGER.warn(method + "/" + message);
	}

	@Override
	public void error(String method, String message) {
		log(method, LEVEL.ERROR, message);
		LOGGER.error(method + "/" + message);
	}

	@Override
	public void setLEVEL(LEVEL level) {
		this.level = level;
	}

	@Override
	public void monitor(Object object) {
		final ObjectMapper mapper = new ObjectMapper();
		String jSonMessage;
		try {
			jSonMessage = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			jSonMessage = object.toString() + ", serializez exception :" + e.getMessage();
			LOGGER.error(e);
		}
		try {
			sendMessage(jSonMessage);
		} catch (Exception e) {
			LOGGER.error(e);
		}

	}
}
