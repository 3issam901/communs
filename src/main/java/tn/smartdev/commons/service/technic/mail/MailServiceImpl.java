package tn.smartdev.commons.service.technic.mail;

import java.util.Properties;

import org.springframework.stereotype.Service;

/**
 * 
 * @author Mohamed Arbi Ben Slimane
 *
 * @Email benslimane.arbi@gmail.com
 *
 * @Date 16 avr. 2017 at 12:48:58
 */
@Service
public class MailServiceImpl implements MailService {

	/**
	 * final attrs
	 */
	private final Properties properties;

	public MailServiceImpl() {
		super();
		this.properties = null;
	}

}
