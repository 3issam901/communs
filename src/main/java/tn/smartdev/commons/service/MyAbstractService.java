package tn.smartdev.commons.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tn.smartdev.commons.email.EmailSenderFacade;
import tn.smartdev.commons.entities.ModelObject;
import tn.smartdev.commons.log.LoggerService;
import tn.smartdev.commons.persistence.BaseDao;

@Component
public abstract class MyAbstractService<T extends ModelObject<S>,S extends Serializable> implements MyService<T,S> {
	protected BaseDao<T,S> dataAccessObject;
	@Autowired
	private EmailSenderFacade gmailSender;
	@Autowired
	private LoggerService loggerService;

	public MyAbstractService(BaseDao<T,S> dataAccessObject) {
		this.dataAccessObject = dataAccessObject;
	}

	public LoggerService getLoggerService() {
		return loggerService;
	}

	public void setLoggerService(LoggerService loggerService) {
		this.loggerService = loggerService;
	}

	public abstract BaseDao<T, S> getDataAccessObject();

	@Override
	public void setDataAccessObject(BaseDao<T,S> dataAccessObject) {
		this.dataAccessObject = dataAccessObject;
	};

	@Override
	public EmailSenderFacade getGmailSender() {
		return gmailSender;
	}

	@Override
	public void setGmailSender(EmailSenderFacade gmailSender) {
		this.gmailSender = gmailSender;
	}

	@Override
	public T get(S primaryKey) {
		return dataAccessObject.get(primaryKey);
	}

	@Override
	public T findById(S primaryKey) {
		return dataAccessObject.findById(primaryKey);
	}

	@Override
	public void persist(T entity) {
		dataAccessObject.persist(entity);
	}

	@Override
	public void delete(T entity) {
		dataAccessObject.delete(entity);
	}

	@Override
	public T save(T entity) {
		return dataAccessObject.save(entity);
	}

	@Override
	public void flush() {
		dataAccessObject.flush();
	}

	@Override
	public void clear() {
		dataAccessObject.clear();

	}

	@Override
	public List<T> findListByCriteria(Map<String, Object> map,
			Integer maxResults, Integer firstResult,BaseDao.DELETION_STATUS status) {
		return dataAccessObject
				.findListByCriteria(map, maxResults, firstResult,status);
	}


	@Override
	public int getCount(Map<String, Object> map,BaseDao.DELETION_STATUS status) {
		return dataAccessObject.getCount(map,status);
	}
}
